#!/bin/bash

# Устанавливаем krew для деплоя mini-operator в наш k8s
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)

# Добавим в PATCH для удобства (лучше сразу прописать в rc)
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Обновим нативно
kubectl krew update
kubectl krew upgrade

# Установим плагин в сам K8S
kubectl krew install minio

# Инициализируем оператора (Важно заранее убедится в том что у нас есть  PVC-провизионер!)
kubectl minio init

# Пробросим порты до Minio-operator с нашего куба до машины
kubectl minio proxy -n minio-operator

# Поднимаем проксю к дашборду minio-svc
kubectl -n minio-operator  port-forward service/minio-svc-console 9090:9090

# Поднимаем прокю к API minio-svc
kubectl -n minio-operator  port-forward service/minio-svc-hl 9000:9000

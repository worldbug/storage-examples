FROM gomicro/goose

ADD ./migrations/*.sql /migrations/
ADD entrypoint.sh /migrations/

ENTRYPOINT ["/migrations/entrypoint.sh"]

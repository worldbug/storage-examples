FROM gcr.io/distroless/static:nonroot-arm64
LABEL maintainer="Kirill Tikhomirov"
USER nonroot:nonroot
COPY --chown=nonroot:nonroot ./app /app
ENTRYPOINT ["/app"]

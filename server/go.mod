module samokater

go 1.16

require (
	github.com/dghubble/sling v1.4.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.2.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/minio/minio-go/v7 v7.0.12
	github.com/sirupsen/logrus v1.8.1
)

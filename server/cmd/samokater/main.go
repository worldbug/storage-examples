package main

import (
	"log"
	"os"
	"samokater/internal/controlers"
	pgdb "samokater/internal/db/postgres"
	"samokater/internal/image_storage/minio_provider"
	"samokater/internal/samokater"
)

func main() {
	fileStorage, err := minio_provider.NewMinioProvider(
		os.Getenv("MINIOHOST"),
		os.Getenv("MINIOUSER"),
		os.Getenv("MINIOPASS"),
		false,
	)
	if err != nil {
		log.Fatalln("Init image_storage error: ", err)
	}

	db, cls, err := pgdb.NewDataBase(os.Getenv("DBPATH"))
	if err != nil {
		log.Fatalln("Init db error: ", err)
	}
	defer cls()

	core, err := samokater.NewSkater(fileStorage, db, os.Getenv("MINIO_ENDPOINT"))
	if err != nil {
		log.Fatalln("Init core error: ", err)
	}

	srv := controlers.NewServer(core)

	err = srv.Run(os.Getenv("SERVERPORT"))
	if err != nil {
		log.Fatalln("Failed to run service: ", err)
	}
}

package db

import "samokater/internal/models"

// DB - База данных, вспомогательная штука
type DB interface {
	CreateNewRentSession(rent models.Rent) error
	StartRentSession(int) error
	CompleteRentSession(int) error
	GetRentSessions(models.User) ([]models.Rent, error)
	GetRentStatus(int) (models.Rent, error)
	AddImageRecord(string, int) error
}

package models

import (
	"io"
	"time"
)

type Rent struct {
	User
	RentInfo
	RentID int
}

type User struct {
	ID   int
	Name string
}

type ImageUnit struct {
	User
	Payload     io.Reader
	PayloadName string
	PayloadSize int64
}

type RentInfo struct {
	StartedAt    time.Time
	CompletedAt  time.Time
	ImagesBefore []string
	ImagesAfter  []string
}

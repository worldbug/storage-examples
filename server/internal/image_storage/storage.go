package image_storage

import (
	"context"
	"samokater/internal/models"
)

// ImageStorage - Интерфейс для работы с хранилищем
type ImageStorage interface {
	Connect() error                                                 // Инициализатор подключения
	UploadFile(context.Context, models.ImageUnit) (string, error)   // Загрузка файлов
	DownloadFile(context.Context, string) (models.ImageUnit, error) // Скачивание файлов
}

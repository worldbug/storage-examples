package samokater

import (
	"context"
	"samokater/internal/db"
	"samokater/internal/image_storage"
	"samokater/internal/models"
)

const ()

type SamokaterCore struct {
	storage            image_storage.ImageStorage
	database           db.DB
	ImageStorageSVCDSN string
}

func NewSkater(storage image_storage.ImageStorage, database db.DB, minioEndpointURL string) (*SamokaterCore, error) {
	return &SamokaterCore{
		storage:            storage,
		database:           database,
		ImageStorageSVCDSN: minioEndpointURL + "/samokater",
	}, nil
}

// Rent Инициализирует сессию аренды для пользователя
func (s *SamokaterCore) Rent(usr models.User) (int, error) {
	rentOpts := models.Rent{
		User:   usr,
		RentID: generateRentID(),
	}

	return rentOpts.RentID, s.database.CreateNewRentSession(rentOpts)
}

// RentStart Начать сессию аренды
func (s *SamokaterCore) RentStart(rentID int) error {
	return s.database.StartRentSession(rentID)
}

// RentStop Завершить сессию аренды
func (s *SamokaterCore) RentStop(rentID int) error {
	return s.database.CompleteRentSession(rentID)
}

// UploadPhoto Загрузить фото из запроса в наш сторадж
func (s *SamokaterCore) UploadPhoto(ctx context.Context, img models.ImageUnit, rentID int) error {
	imgPath, err := s.storage.UploadFile(ctx, img)
	if err != nil {
		return err
	}

	return s.database.AddImageRecord(imgPath, rentID)
}

// RentInfo Возвращает информацию о сессии по ее ID
func (s *SamokaterCore) RentInfo(rentID int) (models.Rent, error) {
	session, err := s.database.GetRentStatus(rentID)
	if err != nil {
		return models.Rent{}, err
	}

	session = enrichImagesLinks(session, s.ImageStorageSVCDSN)
	return session, nil
}

# storage-examples

Пимеры использования объектного хранилища к статье на хабре

## Запуск

1) Сгенерировать базу данных

    `cd migrations && goose sqlite3 ../example.db up`
2) Запустить minio любым удобным способом (см. директорию /manifestos)

    Устновить необходимые env

      | ENV | Влияние | Пример значения|
      |-------|-------|---------|    
      |SERVERPORT| Порт на котором будут приниматься запросы |:8080|
      |DBPATH| Путь к БД (DSN для SQLite/Postgres) |./example.db?parseTime\=true|
      |MINIOHOST| Путь к API Minio |0.0.0.0:9000|
      |MINIOUSER| minio-access-key |minio|
      |MINIOPASS| minio-secret-key |minio123|

## API

| Ручка | Метод | Влияние |Параметры|
|-------|-------|---------|---------|
|/api/v1/rent|`POST`|Создает запись аренды, присваивает ей id|`user`|
|/api/v1/rent_start|`POST`|Помечает сессию аренды как начатую|`rentid`|
|/api/v1/rent_stop|`POST`|Помечает сессию аренды как завершенную|`rentid`|
|/api/v1/upload_photo|`POST`|Присвоить сессии аренды фото пруф|`rentid`|
|/api/v1/download_photo|`GET`|Получить фотопруфы с сессии аренды|`rentid`|

